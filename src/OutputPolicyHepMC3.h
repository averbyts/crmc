#ifndef _OutputPolicyHepMC3_h_
#define _OutputPolicyHepMC3_h_

#include <HepMC3/GenEvent.h>
#include <HepMC3/GenParticle.h>
#include <HepMC3/GenVertex.h>
#include "CRMChepevt.h"
#include "CRMChepmc3.h"
#include <HepMC3/WriterAscii.h>
#include <boost/iostreams/filtering_stream.hpp>

class CRMCoptions;

class OutputPolicyHepMC3
{
public:
  OutputPolicyHepMC3() {}
  void InitOutput(const CRMCoptions& cfg);
  void FillEvent(const CRMCoptions& cfg, const int nEvent);
  void CloseOutput(const CRMCoptions& cfg);
private:
  void PrintTestEvent(const CRMCoptions& cfg);
  boost::iostreams::filtering_ostream *fOut;
  CRMChepevt<HepMC3::GenParticlePtr,
	     HepMC3::GenVertexPtr,
	     HepMC3::FourVector,
	     HepMC3::GenEvent> _hepevt;
  CRMChepmc3 _hepmc3;
  HepMC3::WriterAscii* _writer = 0;
  HepMC3::GenEvent _event;

protected:
  // --------------  test observables
  template <typename T>
  struct Stat {
    T   _sum  = 0;
    T   _sum2 = 0;
    int _cnt  = 0;
    void fill(T x)
    {
      _sum  += x;
      _sum2 += x*x;
      _cnt++;
    }
    double mean() const { return double(_sum) / _cnt; }
    double var() const { return double(_sum2) / _cnt - mean() * mean(); }
    double std() const { return std::sqrt(var()); }
    double sem() const { return std::sqrt(var()/_cnt); }
  };
  Stat<double> _eta;
  Stat<double> _pt;
  Stat<double> _pz;
  Stat<double> _e;
  Stat<int>    _m;
  Stat<int>    _mid;
};


#endif

