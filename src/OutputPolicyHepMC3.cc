#include <OutputPolicyHepMC3.h>

#include <CRMCoptions.h>
#include <CRMCinterface.h>
#include <CRMCconfig.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>

#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

namespace io = boost::iostreams;

//--------------------------------------------------------------------
void OutputPolicyHepMC3::InitOutput(const CRMCoptions& cfg)
{
  fOut = new io::filtering_ostream();
  if (cfg.fOutputMode==CRMCoptions::eHepMC3GZ)
    fOut->push(io::gzip_compressor(io::zlib::best_compression));
  fOut->push(io::file_descriptor_sink(cfg.GetOutputFileName()), std::ios_base::trunc);

  _writer = new HepMC3::WriterAscii(*fOut);
  _hepmc3.init(cfg);
}

//--------------------------------------------------------------------
void OutputPolicyHepMC3::FillEvent(const CRMCoptions& cfg, const int nEvent)
{
  if (!_writer)
    throw std::runtime_error("!!!No HepMC3 ASCII writer defined!");
  if (!_hepevt.convert(_event)) 
    throw std::runtime_error("!!!Could not read next event");

  if (!cfg.fTest) {
    _hepmc3.fillInEvent(cfg, nEvent, _event);
    _writer->write_event(_event);
  }
  // Test mode : compute directly some observables
  int    multiplicity = 0;
  int    plateau      = 0;
  double energy       = 0;
  double pz           = 0;
  for (auto p : _event.particles()) {
    if(p->status() != 1) continue;
    
    multiplicity++;
    double eta = p->momentum().pseudoRapidity();
    double pt  = p->momentum().perp();
      
    if (fabs(eta) < 10000) _eta.fill(eta);
    _pt.fill(pt);
    
    if (fabs(eta) < 0.5) plateau++;
    
    energy += p->momentum().e();
    pz     += p->momentum().pz();
  }
  _e  .fill(energy);
  _pz .fill(pz);
  _m  .fill(multiplicity);
  _mid.fill(multiplicity);
}

//--------------------------------------------------------------------
void OutputPolicyHepMC3::CloseOutput(const CRMCoptions& cfg)
{
  if(cfg.fTest) PrintTestEvent(cfg);

  if (_writer) {
    _writer->close();
    delete _writer;
    delete fOut;
  }
  _writer = 0;
  fOut = 0;
}

//--------------------------------------------------------------------
template <typename T>
std::ostream& operator<<(std::ostream& o,
			 const OutputPolicyHepMC3::Stat<T>& s)
{
  return o << s.mean() << " +/- " << s.sem();
}

//--------------------------------------------------------------------
void OutputPolicyHepMC3::PrintTestEvent(const CRMCoptions& cfg)
{
  std::ostream& o = std::cout;
  o.setf(std::ios::showpoint);
  o.setf(std::ios::fixed);
  o.precision(3);

  if (_m._cnt <= 0) {
    o << "Error during test : no particles !" << std::endl;
    return;
  }
  
  o << "\n          >> Test output <<\n\n"
    << "  Total Cross Section (mb):      " << gCRMC_data.sigtot << "\n"
    << "  Elastic Cross Section (mb):    " << gCRMC_data.sigela << "\n"
    << "  Inel. Cross Section (mb) :     " << gCRMC_data.sigine << "\n" ;
  if(cfg.fProjectileId>1 || cfg.fTargetId>1)
    o << "  Inel. AA Cross Section (mb):   " << gCRMC_data.sigineaa << "\n"
      << "  Elastic AA Cross Section (mb): " << gCRMC_data.sigelaaa << "\n"
      << "  Total AA Cross Section (mb):   " << gCRMC_data.sigtotaa << "\n" ;
  
  o << "\n"
    << "  Energy (GeV):                  " << _e    << "\n"
    << "  Long. Momentum (GeV/c):        " << _pz   << "\n"
    << "  Multiplicity:                  " << _m    << "\n"
    << "  PlateauHeight:                 " << _mid  << "\n"
    << "  MeanPseudorapidity:            " << _eta  << "\n"
    << "  MeanPt (GeV/c):                " << _pt   << "\n"
    << std::endl;
}

//--------------------------------------------------------------------
//
// EOF
//
