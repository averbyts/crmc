#ifndef _OutputPolicyRivet_h_
#define _OutputPolicyRivet_h_

#include <HepMC3/GenEvent.h>
#include <HepMC3/GenParticle.h>
#include <HepMC3/GenVertex.h>
#include <Rivet/AnalysisHandler.hh>
#include "CRMChepevt.h"
#include "CRMChepmc3.h"
class CRMCoptions;

class OutputPolicyRivet
{
public:
  OutputPolicyRivet();
  void InitOutput(const CRMCoptions& cfg);
  void FillEvent(const CRMCoptions& cfg, const int nEvent);
  void CloseOutput(const CRMCoptions& cfg);
private:
  void PrintTestEvent(const CRMCoptions& cfg);
  CRMChepevt<HepMC3::GenParticlePtr,
	     HepMC3::GenVertexPtr,
	     HepMC3::FourVector,
	     HepMC3::GenEvent> _hepevt;
  CRMChepmc3 _hepmc3;
  HepMC3::GenEvent _event;
  Rivet::AnalysisHandler _handler;
  bool _is_init = false;
};


#endif

