#ifndef _OutputPolicyHepMC_h_
#define _OutputPolicyHepMC_h_

#include <HepMC/GenEvent.h>
#include <HepMC/GenParticle.h>
#include <HepMC/GenVertex.h>
#include "CRMChepevt.h"

class CRMCoptions;

namespace HepMC {
  class IO_GenEvent;
}

#include <boost/iostreams/filtering_stream.hpp>


class OutputPolicyHepMC {

 public:
  OutputPolicyHepMC();

  void InitOutput(const CRMCoptions& cfg);
  void FillEvent(const CRMCoptions& cfg, const int nEvent);
  void CloseOutput(const CRMCoptions& cfg);

 private:

  void PrintTestEvent(const CRMCoptions& cfg);
  boost::iostreams::filtering_ostream *fOut;
  CRMChepevt<HepMC::GenParticle*,
	     HepMC::GenVertex*,
	     HepMC::FourVector,
	     HepMC::GenEvent> hepevt;
  HepMC::IO_GenEvent* ascii_out;

 protected:
  // --------------  test observables
  template <typename T>
  struct Stat {
    T   _sum  = 0;
    T   _sum2 = 0;
    int _cnt  = 0;
    void fill(T x)
    {
      _sum  += x;
      _sum2 += x*x;
      _cnt++;
    }
    double mean() const { return double(_sum) / _cnt; }
    double var() const { return double(_sum2) / _cnt - mean() * mean(); }
    double std() const { return std::sqrt(var()); }
    double sem() const { return std::sqrt(var()/_cnt); }
  };
  Stat<double> _eta;
  Stat<double> _pt;
  Stat<double> _pz;
  Stat<double> _e;
  Stat<int>    _m;
  Stat<int>    _mid;
};


#endif

