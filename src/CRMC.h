#ifndef __CRMC_H
#define __CRMC_H

#include <CRMCinterface.h>
//#include <CRMCfilter.h>

// //////////
// //////////

class CRMCoptions;


template<class OutputPolicy>
class CRMC : public OutputPolicy {

  CRMC();

 public:
  CRMC(const CRMCoptions& cfg);

  bool init();
  bool run();
  bool finish();

  CRMCinterface& GetInterface() { return fInterface; }

 private:

  const CRMCoptions& fCfg;
  CRMCinterface fInterface;

  //CRMCfilter fFilter;

};



#ifdef WITH_ROOT
#include <OutputPolicyROOT.h>
typedef CRMC<OutputPolicyROOT> CRMC2ROOT;
#endif

#ifdef WITH_HEPMC
#include <OutputPolicyHepMC.h>
typedef CRMC<OutputPolicyHepMC> CRMC2HepMC;
#endif

#ifdef WITH_HEPMC3
#include <OutputPolicyHepMC3.h>
typedef CRMC<OutputPolicyHepMC3> CRMC2HepMC3;
#endif

#ifdef WITH_RIVET
#include <OutputPolicyRivet.h>
typedef CRMC<OutputPolicyRivet> CRMC2Rivet;
#endif

#include <OutputPolicyLHE.h>
typedef CRMC<OutputPolicyLHE> CRMC2LHE;

#include <OutputPolicyNone.h>
typedef CRMC<OutputPolicyNone> CRMC2NONE;


#endif
