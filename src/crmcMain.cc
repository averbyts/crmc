#include <CRMCoptions.h>
#include <CRMC.h>
#ifdef WITH_ROOT
#include <OutputPolicyROOT.h>
#endif
#ifdef WITH_HEPMC3
#include <OutputPolicyHepMC3.h>
#endif
#ifdef WITH_HEPMC
#include <OutputPolicyHepMC.h>
#endif
#ifdef WITH_RIVET
#include <OutputPolicyRivet.h>
#endif
#include <OutputPolicyLHE.h>
#include <OutputPolicyNone.h>

#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;



int
main(int argc, char **argv)
{

  const CRMCoptions cfg(argc, argv);
  if (cfg.OptionsError()){
    cout << "\nConfiguration Error\n" << endl;
    return 2;
  }

  switch(cfg.GetOutputMode()) {


#ifdef WITH_ROOT
  case CRMCoptions::eROOT:
    {
      CRMC2ROOT crmc(cfg);
      crmc.init();
      crmc.run();
      crmc.finish();
    }
    break;
#endif

#ifdef WITH_HEPMC
  case CRMCoptions::eHepMC:
  case CRMCoptions::eHepMCGZ:
    {
      CRMC2HepMC crmc(cfg);
      crmc.init();
      crmc.run();
      crmc.finish();
    }
    break;
#endif

#ifdef WITH_HEPMC3
  case CRMCoptions::eHepMC3:
  case CRMCoptions::eHepMC3GZ:
    {
      CRMC2HepMC3 crmc(cfg);
      crmc.init();
      crmc.run();
      crmc.finish();
    }
    break;
#endif

#ifdef WITH_RIVET
  case CRMCoptions::eRivet:
    {
      CRMC2Rivet crmc(cfg);
      crmc.init();
      crmc.run();
      crmc.finish();
    }
    break;
#endif

  case CRMCoptions::eLHE:
  case CRMCoptions::eLHEGZ:
    {
      CRMC2LHE crmc(cfg);
      crmc.init();
      crmc.run();
      crmc.finish();
    }
    break;

  case CRMCoptions::eNone:
    {
      CRMC2NONE crmc(cfg);
      crmc.init();
      crmc.run();
      crmc.finish();
    }
    break;

  }

  // default options
}
