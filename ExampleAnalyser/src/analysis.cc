#include <algorithm>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>
#include <stdlib.h>

#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

#include "HepMC3/GenEvent.h"
#include "HepMC3/ReaderAscii.h"
#include "HepMC3/WriterAscii.h"
#include "HepMC3/Print.h"

#include <TFile.h>
#include <TH1D.h>

#include "analysis.h"

namespace io = boost::iostreams;
using namespace std;


int main (int argc, char **argv)
{
  if (argc==1) {
    cout << "\n Please, provide input data file(s) in hepmc or hepmcgz format\n" << endl;
    return 0;
  }
  
  //-------------------SET UP DATA
  TFile* theOutFile;
  string outFileName ("new_histogram_file.root");
  cout << " ! Opening output file: " << outFileName << endl;
  theOutFile = new TFile (outFileName.c_str(),"RECREATE");

  vector<string> files;
  for (int ia=1; ia<argc; ++ia) {
    files.push_back(argv[ia]);
  }
  DataManager data;
  data.SetFiles (files); //for more models, loop over models and call SetFiles each time
  theOutFile->mkdir ("model1");

  //------------------SET UP HISTOGRAMS
  TH1D* exampleHist = new TH1D ("dNdeta",";#eta;dN/d#eta",21,-10,10);


  //-------------------EVENT LOOP
  int nEvts = 0;
  while (data.GetNextEvent ())
    {
      ++nEvts;

      //-------------------PARTICLE LOOP
      auto par = data.evt.particles().begin ();
      for (; par != data.evt.particles().end (); ++par)
	{
	  auto const& p = *par; 
	  if (p->status () != 1) continue; //get final state particles. status == 2 are decayed particles, status == 4 is beam particles

	  //HepMC3::GenVertex* parent_vertex = p->production_vertex();
	  //const int id = p->pdg_id ();
	  const double eta = p->momentum ().eta ();
	  //const double pt = p->momentum ().perp ();
	  //const double e = p->momentum ().e ();

          //for more advance paramters see HepMC documentation or load #include <TParticle.h> and fill object (see analysis.h)

          //-------------------EVENT SELECTION
          //-------------------FILL HISTOGRAMS WITH PER PARTICLE VARIABLES
          exampleHist->Fill (eta);
	}//PARTICLE LOOP
      //-------------------FILL HISTOGRAMS WITH PER EVENT VARIABLES
    }//EVENT LOOP
    //---------------FINALISE HISTOGRAMS
  exampleHist->Scale (1. / nEvts, "width");

  //----------------Closing Files
  std::cout << " ! Writing output file: " << outFileName << std::endl;
  theOutFile->Write();
  delete exampleHist;
  exampleHist = 0;
  std::cout << " ! Closing output file: " << outFileName << std::endl;
  theOutFile->Close();
  delete theOutFile;
  theOutFile = 0;
  return 0;
}
